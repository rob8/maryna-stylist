<div class="sidebar">
							
	<div class="sidebar-inner">
		
		<?php $search_text = "Search"; ?> 
		<form method="get" id="searchform" action="<?php bloginfo('home'); ?>/"> 
			<div class="form-group">
			<input type="text" class="form-search form-control" value="<?php echo $search_text; ?>" name="s" id="s" onblur="if (this.value == '') {this.value = '<?php echo $search_text; ?>';}" onfocus="if (this.value == '<?php echo $search_text; ?>') {this.value = '';}" /> 
			<input type="hidden" id="searchsubmit" /> 
			</div>
		</form>
		
		<h4>Categories</h4>
		<div class="form-group">
			<?php wp_dropdown_categories( 'show_option_none=Filter By Category&class=form-control' ); ?>
			<script type="text/javascript">
				<!--
				var dropdown = document.getElementById("cat");
				function onCatChange() {
					if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
						location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
					}
				}
				dropdown.onchange = onCatChange;
				-->
			</script>
		</div>
		
		<h4>Archives</h4>
		
		<div class="form-group">
		<select name="archive-dropdown" class="form-control" onchange="document.location.href=this.options[this.selectedIndex].value;">
		  <option value=""><?php echo esc_attr( __( 'Filter By Month' ) ); ?></option> 
		  <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
		</select>
		</div>
		
		<h4>Authors</h4>
		<?php
		$args = array('selected' => 'false', 'name' => 'display_name');
		$blogusers = get_users($args);
		?>
		
		<form action="<?php bloginfo('url'); ?>" method="get">
		<select id="select-of-tags" name="author" class="form-control" onchange="if(this.selectedIndex){location.href=(this.options[selectedIndex].value)}">
		<option value="">View Authors…</option>
		<?php foreach ($blogusers as $user) {?>
		<option value="<?php echo get_author_posts_url($user->ID); ?>"><?php echo $user->display_name ?></option>
		<?php } ?>
		</select>
		</form>
		
	</div> <!-- sidebar-inner -->
	
</div> <!-- sidebar -->