		<div id="home-contact">
		
			<div class="container">
				
				
				
				<div class="row">
					
					<div class="col-xs-12 col-md-8">
								
						<h2>Get <strong>In Touch</strong></h2>
	
						<?php if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 1 ); } ?>
						
					</div>
					
					<div class="col-xs-12 col-md-3 col-md-offset-1">
						
						<h2>On <strong>Social</strong></h2>
						<ul class="social">
								<?php echo ledda_social_links(array('facebook','twitter','pinterest','linkedin','google-plus','instagram','youtube')); ?>
						</ul>
						<br/>
						<h2>By <strong>Phone</strong></h2>
						<p class="phone-num lead">(423) 402-0570</p>
						
					</div>
			
				</div>
			
			</div>
			
		</div> <!-- #home-contact -->
		
		<?php wp_footer(); ?>
		
		<script type="text/javascript">
			
			jQuery(function() {
			  jQuery('a[href*="#"]:not([href="#"])').click(function() {
			    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			      var target = jQuery(this.hash);
			      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
			      if (target.length) {
			        jQuery('html, body').animate({
			          scrollTop: target.offset().top
			        }, 1000);
			        return false;
			      }
			    }
			  });
			  
			  jQuery('.owl-carousel').owlCarousel({
				    loop:true,
				    margin:30,
				    dots:true,
				    responsive:{
				        0:{
				            items:1
				        },
				        550:{
				            items:3
				        },
				        1000:{
				            items:4
				        }
				    }
				})
				
				$('.nav a').on('click', function(){
				    //$('.btn-navbar').click(); //bootstrap 2.x
				    $('.navbar-toggle').click() //bootstrap 3.x by Richard
				});
				$('.navbar').on('mouseleave', function(){
				    //$('.btn-navbar').click(); //bootstrap 2.x
				    $('.navbar-collapse').removeClass('in') //bootstrap 3.x by Richard
				});
			});
			
		</script>
		
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-87772906-1', 'auto');
		  ga('send', 'pageview');
		
		</script>
		
	</body>
	
	
</html>