<!DOCTYPE html>
<html lang="en">
	
	
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		
		<?php wp_head(); //Loads head items generated by Wordpress ?>
		
		<script src="https://use.typekit.net/rkn0rqo.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
		
		<script>
			$(document).ready(function(){
				$('.owl-carousel').owlCarousel({
				    loop:true,
				    margin:20,
				    stagePadding: 20,
				    responsiveClass:true,
				    responsive:{
				        0:{
				            items:2,
				            nav:false
				        },
				        600:{
				            items:3,
				            nav:false
				        },
				        1000:{
				            items:6,
				            loop:false
				        }
				    }
				})
			});
		</script>
		
		<meta name="theme-color" content="#EE4877">
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	
	
	<body <?php body_class( $class ); ?>> 
		<div class="container">
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 single-work-logo">
					<a href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo('template_directory') ?>/images/logo-single-work.png" alt="logo-single-work" width="1078" height="180" class="img-responsive"></a>
				</div>
			</div>
			
			<div class="row">
				
				<div class="col-xs-10 col-xs-offset-1 col-md-12 col-md-offset-0">
					
					<div class="row">
			
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
				
					<div class="col-md-6 col-md-offset-3 single-work-image">
						
						<div class="inner">
							
							<div class="inner-content">
						
								<?php 
									if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
										$params = array( 'width' => 800, 'height' => 800 );
										echo "<img src='" . bfi_thumb( get_the_post_thumbnail_url($post->ID, full), $params ) . "' class='img-responsive' />";
									} 
								?>
						
							</div>
						
						</div>
						
						<?php $quote = get_post_meta($post->ID, 'maryna_work_quote', true); ?>
						<?php echo ( !empty($quote) ? '<div class="quote"><blockquote><p>'.$quote.'</p></blockquote></div>' : '' ); ?>
						
					
						
					</div>
				
			
				<?php $workid = $post->ID; ?>
			
			<?php endwhile; else: ?>
			<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
			<?php endif; ?>
			
					</div></div></div>
			
			<div class="row">
				
				<div class="col-md-6 col-md-offset-3 single-work-buttons text-center">
					<a href="<?php echo get_home_url(); ?>" class="btn btn-lg btn-primary">Learn More</a>
				</div>
				
			</div>
			
		</div> <!-- container -->
		
		<!-- footer -->
		<footer class="dashFooterBox">
			<div class="dashFooterBoxInner">
				<!-- footer button -->
				<div class="dashFooterBtn">
					<label for="show-foot">More Work</label>
				</div>
				<!-- end footer button -->
				<!-- footer -->
				<input type="checkbox" id="show-foot" name="show-the-footer" class="r-foot-check">
				<nav class="dashFooter">
					<div class="default-width">
						<div class="dashFooterInner">
							<div class="owl-carousel owl-theme">
								
								<?php 
									
									
									
									$args = array( 'posts_per_page' => 20, 'post_type' => 'work', 'orderby' => 'menu_order' );
									
									$myservices = get_posts( $args );
									
									foreach ( $myservices as $post ) : setup_postdata( $post ); ?>
										<div class="owl-item"><div class="inner<?php echo ( ($workid == $post->ID) ? ' active' : '' ); ?>">
											<a class="inner-content" href="<?php the_permalink(); ?>">
												
												<?php 
													if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
														$params = array( 'width' => 550, 'height' => 550 );
														echo "<img src='" . bfi_thumb( get_the_post_thumbnail_url($post->ID, full), $params ) . "' class='img-responsive' />";
													} 
												?>
																					
											</a> <!-- .inner-content -->
										</div></div>
									<?php endforeach; 
								wp_reset_postdata(); ?>
							</div>
						</div>
					</div>
				</nav>
			<!-- end footer -->
			</div>
		</footer>
		
		<?php get_footer(); ?>
		
	</body>
	
</html>