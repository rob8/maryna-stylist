<?php
	
	error_reporting(E_ERROR);

	// Get theme options.
	GLOBAL $ledda_options;
	$ledda_options = get_option( 'ledda_options' );

	// Add metabox framework
	if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
		require_once dirname( __FILE__ ) . '/cmb2/init.php';
	} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
		require_once dirname( __FILE__ ) . '/CMB2/init.php';
	}

	// Register Custom Navigation Walker
	require_once('wp_bootstrap_navwalker.php');
	
	require_once('BFI_Thumb.php');	
	
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'title-tag' );
	
	
	if ( function_exists( 'register_nav_menus' ) ) {
		register_nav_menus(
			array(
			  'primary_nav' => 'Primary Navigation',
			  'footer_nav' => 'Footer Navigation'
			)
		);
	}
	
	
	// Add styles and scripts

	function ledda_scripts() {
		wp_enqueue_style( 'fontawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );
		wp_enqueue_style( 'ledda-style', get_stylesheet_uri(), '', '2.1' );
		wp_enqueue_style( 'hover', get_template_directory_uri() . '/css/hover-min.css' );
		wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js', array('jquery'), '1.0.0', true );
		wp_enqueue_script( 'matchHeight', get_template_directory_uri() . '/js/jquery.matchHeight-min.js', array('jquery'), '1.0.0', true );
		wp_enqueue_script( 'owlCarousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.0.0', true );
		wp_enqueue_style( 'owlCarousel', get_template_directory_uri() . '/css/owl.carousel.css' );
		//wp_enqueue_style( 'owlCarouselTheme', get_template_directory_uri() . '/bower_components/owl.carousel/dist/assets/owl.theme.default.min.css' );
		//wp_enqueue_script('masonry');
		
		if (!is_admin()) {
			// comment out the next two lines to load the local copy of jQuery
			wp_deregister_script('jquery');
			wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', false, '1.11.3');
			wp_enqueue_script('jquery');
		}
	}
		
	add_action( 'wp_enqueue_scripts', 'ledda_scripts' );
	
	
// Add favicon and apple icons

	add_action('wp_head', 'ledda_favicons');
	
	function ledda_favicons() {
		
		$favicons = array (
            'apple-touch-icon-57x57' => array (
                'rel' => 'apple-touch-icon',
                'type' => '',
                'sizes' => '57x57',
                'filename' => 'apple-touch-icon-57x57.png'
            ),
            'apple-touch-icon-60x60' => array (
                'rel' => 'apple-touch-icon',
                'type' => '',
                'sizes' => '60x60',
                'filename' => 'apple-touch-icon-60x60.png'
            ),
            'apple-touch-icon-72x72' => array (
                'rel' => 'apple-touch-icon',
                'type' => '',
                'sizes' => '72x72',
                'filename' => 'apple-touch-icon-72x72.png'
            ),
            'apple-touch-icon-76x76' => array (
                'rel' => 'apple-touch-icon',
                'type' => '',
                'sizes' => '76x76',
                'filename' => 'apple-touch-icon-76x76.png'
            ),
            'apple-touch-icon-114x114' => array (
                'rel' => 'apple-touch-icon',
                'type' => '',
                'sizes' => '114x114',
                'filename' => 'apple-touch-icon-114x114.png'
            ),
            'apple-touch-icon-120x120' => array (
                'rel' => 'apple-touch-icon',
                'type' => '',
                'sizes' => '120x120',
                'filename' => 'apple-touch-icon-120x120.png'
            ),
            'apple-touch-icon-144x144' => array (
                'rel' => 'apple-touch-icon',
                'type' => '',
                'sizes' => '144x144',
                'filename' => 'apple-touch-icon-144x144.png'
            ),
            'apple-touch-icon-152x152' => array (
                'rel' => 'apple-touch-icon',
                'type' => '',
                'sizes' => '152x152',
                'filename' => 'apple-touch-icon-152x152.png'
            ),
            'apple-touch-icon-180x180' => array (
                'rel' => 'apple-touch-icon',
                'type' => '',
                'sizes' => '180x180',
                'filename' => 'apple-touch-icon-180x180.png'
            ),
            'favicon-32x32' => array (
                'rel' => 'icon',
                'type' => 'image/png',
                'sizes' => '32x32',
                'filename' => 'favicon-32x32.png'
            ),
            'android-chrome-192x192' => array (
                'rel' => 'icon',
                'type' => 'image/png',
                'sizes' => '192x192',
                'filename' => 'android-chrome-192x192.png'
            ),
            'favicon-96x96' => array (
                'rel' => 'icon',
                'type' => 'image/png',
                'sizes' => '96x96',
                'filename' => 'favicon-96x96.png'
            ),
            'favicon-16x16' => array (
                'rel' => 'icon',
                'type' => 'image/png',
                'sizes' => '16x16',
                'filename' => 'favicon-16x16.png'
            )
		);
		
		foreach ($favicons as $favicon) {
			echo '<link'.( !empty($favicon['rel']) ? ' rel="'.$favicon['rel'].'"' : '' ).( !empty($favicon['type']) ? ' type="'.$favicon['type'].'"' : '' ).( !empty($favicon['sizes']) ? ' sizes="'.$favicon['sizes'].'"' : '' ).( !empty($favicon['filename']) ? ' href="'.get_template_directory_uri().'/images/'.$favicon['filename'].'"' : '' ).' />
';
		}
	}

// Lets restrict access to only logged-in users.

	function password_protected() {
		
		GLOBAL $ledda_options;
		
		if ( !is_user_logged_in() && ($ledda_options['devmode'] == "enabled") ) {
			auth_redirect();
		}
	}
	
	add_action('template_redirect', 'password_protected');
	add_action('do_feed', 'password_protected');
	
	
	function ledda_copyright() {
		GLOBAL $ledda_options;
		$output = '';
		
		$output .= ( !empty($ledda_options[copyright_text]) ? $ledda_options[copyright_text] : '' );
		return $output;
	}
	
	
	//Social list navigation generation
	function ledda_social_links($accounts){
		
		GLOBAL $ledda_options;
		$output = '';
		
		foreach ($accounts as $account) {
			switch ($account) {
			    case "facebook":
			        $output .= (!empty($ledda_options[facebook_url]) ? '<li class="facebook"><a class="hvr-grow" href="'.$ledda_options[facebook_url].'" target="_blank"><i class="fa fa-facebook-square"></i> Facebook</a></li>' : '<li class="facebook" style="display:none;"></li>');
			        break;
			    case "twitter":
			        $output .= (!empty($ledda_options[twitter_url]) ? '<li class="twitter"><a class="hvr-grow" href="'.$ledda_options[twitter_url].'" target="_blank"><i class="fa fa-twitter-square"></i> Twitter</a></li>' : '<li class="twitter" style="display:none;"></li>');
			        break;
			    case "google-plus":
			        $output .= (!empty($ledda_options[gplus_url]) ? '<li class="google-plus"><a class="hvr-grow" href="'.$ledda_options[gplus_url].'" target="_blank"><i class="fa fa-google-plus-square"></i> Google Plus</a></li>' : '<li class="google-plus" style="display:none;"></li>');
			        break;
			    case "instagram":
			        $output .= (!empty($ledda_options[instagram_url]) ? '<li class="instagram"><a class="hvr-grow" href="'.$ledda_options[instagram_url].'" target="_blank"><i class="fa fa-instagram"></i> Instagram</a></li>' : '<li class="instagram" style="display:none;"></li>');
			        break;
			    case "youtube":
			        $output .= (!empty($ledda_options[youtube_url]) ? '<li class="youtube"><a class="hvr-grow" href="'.$ledda_options[youtube_url].'" target="_blank"><i class="fa fa-youtube-square"></i> YouTube</a></li>' : '<li class="youtube" style="display:none;"></li>');
			        break;
			    default:
			        break;
			}
		}
		
		return $output;
	}


function ledda_get_array($type) {
	$output = '';

	if ( $type == "devmodes" ) {
		$dropdown_devmodes = array (
		    'one' => array (
		        'label' => 'Do not restrict site access.',
		        'value' => ''
		    ),
		    'two' => array (
		        'label' => 'Restrict access to authenticated users.',
		        'value' => 'enabled'
		    )
		);
		
		foreach ($dropdown_devmodes as $possible_answer) {
			$output["{$possible_answer['value']}"] = $possible_answer['label'];
		}
	}
	
	return $output;
}
	
	
	/**
 * Contains methods for customizing the theme customization screen.
 * 
 * @link http://codex.wordpress.org/Theme_Customization_API
 * @since ledda 1.0
 */
class ledda_customize {
   /**
    * This hooks into 'customize_register' (available as of WP 3.4) and allows
    * you to add new sections and controls to the Theme Customize screen.
    * 
    * Note: To enable instant preview, we have to actually write a bit of custom
    * javascript. See live_preview() for more.
    *  
    * @see add_action('customize_register',$func)
    * @param \WP_Customize_Manager $wp_customize
    * @link http://ottopress.com/2012/how-to-leverage-the-theme-customizer-in-your-own-themes/
    * @since ledda 1.0
    */
	public static function register ( $wp_customize ) {
		//1. Define a new section (if desired) to the Theme Customizer
		$wp_customize->add_section( 'social_options', 
			array(
				'title' => __( 'Social Media Accounts', 'ledda' ), //Visible title of section
				'priority' => 35, //Determines what order this appears in
				'capability' => 'edit_theme_options', //Capability needed to tweak
				'description' => __('Add URLs for your social media accounts to include them in the theme.', 'ledda'), //Descriptive tooltip
			) 
		);
		
		$wp_customize->add_section( 'misc_options', 
			array(
				'title' => __( 'Misc Options', 'ledda' ), //Visible title of section
				'priority' => 35, //Determines what order this appears in
				'capability' => 'edit_theme_options', //Capability needed to tweak
				'description' => __('Misc settings for the theme.', 'ledda'), //Descriptive tooltip
			) 
		);
		
		//2. Register new settings to the WP database...
		
		
		//Dev mode
			$wp_customize->add_setting('ledda_options[devmode]', array(
				'capability' => 'edit_theme_options',
				'type'       => 'option',
				//'default'       => '', # Default custom text
			));
	
			$wp_customize->add_control('ledda_options[devmode]', array(
				'label' => 'Enable Dev Mode?', # Label of text form
				'description' => __( 'Enabling dev mode will restrict access to the site to logged-in users.', 'ledda' ),
				'section' => 'misc_options', # Layout Section
				'type' => 'select', # Type of control: text input
				'choices' => ledda_get_array('devmodes')
			));
		
		//Facebook
		
			$wp_customize->add_setting('ledda_options[facebook_url]', array(
				'capability' => 'edit_theme_options',
				'type'       => 'option',
				'default'       => '', # Default custom text
			));
			
			$wp_customize->add_control('ledda_options[facebook_url]', array(
				'label' => 'Facebook URL', # Label of text form
				'section' => 'social_options', # Layout Section
				'type' => 'text', # Type of control: text input
			));
			
		//Twitter
		
			$wp_customize->add_setting('ledda_options[twitter_url]', array(
				'capability' => 'edit_theme_options',
				'type'       => 'option',
				'default'       => '', # Default custom text
			));
			
			$wp_customize->add_control('ledda_options[twitter_url]', array(
				'label' => 'Twitter URL', # Label of text form
				'section' => 'social_options', # Layout Section
				'type' => 'text', # Type of control: text input
			));
			
		//YouTube
		
			$wp_customize->add_setting('ledda_options[youtube_url]', array(
				'capability' => 'edit_theme_options',
				'type'       => 'option',
				'default'       => '', # Default custom text
			));
			
			$wp_customize->add_control('ledda_options[youtube_url]', array(
				'label' => 'YouTube URL', # Label of text form
				'section' => 'social_options', # Layout Section
				'type' => 'text', # Type of control: text input
			));
			
		//Google Plus
		
			$wp_customize->add_setting('ledda_options[gplus_url]', array(
				'capability' => 'edit_theme_options',
				'type'       => 'option',
				'default'       => '', # Default custom text
			));
			
			$wp_customize->add_control('ledda_options[gplus_url]', array(
				'label' => 'Google Plus URL', # Label of text form
				'section' => 'social_options', # Layout Section
				'type' => 'text', # Type of control: text input
			));
			
		//Instagram
		
			$wp_customize->add_setting('ledda_options[instagram_url]', array(
				'capability' => 'edit_theme_options',
				'type'       => 'option',
				'default'       => '', # Default custom text
			));
			
			$wp_customize->add_control('ledda_options[instagram_url]', array(
				'label' => 'Instagram URL', # Label of text form
				'section' => 'social_options', # Layout Section
				'type' => 'text', # Type of control: text input
			));
			
		//Copyright Text
		
			$wp_customize->add_setting('ledda_options[copyright_text]', array(
				'capability' => 'edit_theme_options',
				'type'       => 'option',
				'default'       => '', # Default custom text
			));
			
			$wp_customize->add_control('ledda_options[copyright_text]', array(
				'label' => 'Copyright Text', # Label of text form
				'section' => 'misc_options', # Layout Section
				'type' => 'text', # Type of control: text input
			));
			
				
		//Stuff
		
			$wp_customize->get_setting( 'facebook_url' )->transport = 'postMessage';
			$wp_customize->get_setting( 'twitter_url' )->transport = 'postMessage';
			$wp_customize->get_setting( 'linkedin_url' )->transport = 'postMessage';
			$wp_customize->get_setting( 'youtube_url' )->transport = 'postMessage';
			$wp_customize->get_setting( 'gplus_url' )->transport = 'postMessage';
			$wp_customize->get_setting( 'instagram_url' )->transport = 'postMessage';
			$wp_customize->get_setting( 'pinterest_url' )->transport = 'postMessage';
			$wp_customize->get_setting( 'copyright_text' )->transport = 'postMessage';
		
	}
	
	public static function live_preview() {
		wp_enqueue_script( 
           'ledda-themecustomizer', // Give the script a unique ID
           get_template_directory_uri() . '/js/customizer.js', // Define the path to the JS file
           array(  'jquery', 'customize-preview' ), // Define dependencies
           '', // Define a version (optional) 
           true // Specify whether to put in footer (leave this true)
      );
	}
	

}

// Setup the Theme Customizer settings and controls...
add_action( 'customize_register' , array( 'ledda_customize' , 'register' ) );

// Enqueue live preview javascript in Theme Customizer admin screen
add_action( 'customize_preview_init' , array( 'ledda_customize' , 'live_preview' ) );

// Register a custom post type for Services

add_action('init', 'my_services');
function my_services() 
{
  $labels = array(
    'name' => _x('Services', 'post type general name'),
    'singular_name' => _x('Service', 'post type singular name'),
    'add_new' => _x('Add Service', 'book'),
    'add_new_item' => __('Add New Service'),
    'edit_item' => __('Edit Service'),
    'new_item' => __('New Service'),
    'view_item' => __('View Service'),
    'search_items' => __('Search Services'),
    'not_found' =>  __('No Services found'),
    'not_found_in_trash' => __('No Services found in Trash'), 
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => false,
    'publicly_queryable' => false,
    'show_ui' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'menu_icon' => 'dashicons-products',
    'supports' => array('title','thumbnail','editor','page-attributes')
  ); 
  register_post_type('services',$args);
}

add_action( 'cmb2_admin_init', 'maryna_register_services_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
 */
function maryna_register_services_metabox() {
	$prefix = 'maryna_services_';
	
	$cmb_services = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => __( 'Service Options', 'cmb2' ),
		'object_types'  => array( 'services', ), // Post type
		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		// 'show_names' => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
	) );
	
	$cmb_services->add_field( array(
		'name' => __( 'Service Icon', 'cmb2' ),
		'desc' => __( 'Upload an image or enter a URL.', 'cmb2' ),
		'id'   => $prefix . 'image',
		'type' => 'file',
	) );
	
	$cmb_services->add_field( array(
		'name' => __( 'Short Description (for box)', 'cmb2' ),
		'desc' => __( 'Enter a short description for display on the homepage.', 'cmb2' ),
		'id'   => $prefix . 'shortdesc',
		'type' => 'textarea',
	) );
}

add_action('init', 'my_work');
function my_work() 
{
  $labels = array(
    'name' => _x('Recent Work', 'post type general name'),
    'singular_name' => _x('Work', 'post type singular name'),
    'add_new' => _x('Add Work', 'book'),
    'add_new_item' => __('Add New Work'),
    'edit_item' => __('Edit Work'),
    'new_item' => __('New Work'),
    'view_item' => __('View Work'),
    'search_items' => __('Search Work'),
    'not_found' =>  __('No Work found'),
    'not_found_in_trash' => __('No Work found in Trash'), 
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => false,
    'publicly_queryable' => false,
    'has_archive' => true,
    'show_ui' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'menu_icon' => 'dashicons-camera',
    'supports' => array('title','thumbnail')
  ); 
  register_post_type('work',$args);
}


add_action( 'cmb2_admin_init', 'maryna_register_work_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
 */
function maryna_register_work_metabox() {
	$prefix = 'maryna_work_';
	
	$cmb_services = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => __( 'Work Options', 'cmb2' ),
		'object_types'  => array( 'work', ), // Post type
		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		// 'show_names' => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
	) );
	
	$cmb_services->add_field( array(
		'name' => __( 'Testimonial Quote', 'cmb2' ),
		'desc' => __( 'Quote for use on the homepage, modal box, and individual page.', 'cmb2' ),
		'id'   => $prefix . 'quote',
		'type' => 'textarea',
	) );
}

?>