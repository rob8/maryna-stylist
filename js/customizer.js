/**
 * This file adds some LIVE to the Theme Customizer live preview. To leverage
 * this, set your custom settings to 'postMessage' and then add your handling
 * here. Your javascript should grab settings from customizer controls, and 
 * then make any necessary changes to the page using jQuery.
 */
( function( $ ) {
 
	// Update the Facebook link in real time...
	wp.customize( 'ledda_options[facebook_url]', function( value ) {
		value.bind( function( to ) {
			$( '.social li.facebook' ).html( to ? "<a href=" + to + " target='_blank'><i class='fa fa-facebook-square'></i></a>" : "" )
			$( '.social li.facebook' ).css('display', to ? 'inline-block' : 'none' );
		} );
	} );
	
	// Update the Twitter link in real time...
	wp.customize( 'ledda_options[twitter_url]', function( value ) {
		value.bind( function( to ) {
			$( '.social li.twitter' ).html( to ? "<a href=" + to + " target='_blank'><i class='fa fa-twitter-square'></i></a>" : "" )
			$( '.social li.twitter' ).css('display', to ? 'inline-block' : 'none' );
		} );
	} );
	
	// Update the Pinterest link in real time...
	wp.customize( 'ledda_options[pinterest_url]', function( value ) {
		value.bind( function( to ) {
			$( '.social li.pinterest' ).html( to ? "<a href=" + to + " target='_blank'><i class='fa fa-pinterest-square'></i></a>" : "" )
			$( '.social li.pinterest' ).css('display', to ? 'inline-block' : 'none' );
		} );
	} );
	
	// Update the LinkedIn link in real time...
	wp.customize( 'ledda_options[linkedin_url]', function( value ) {
		value.bind( function( to ) {
			$( '.social li.linkedin' ).html( to ? "<a href=" + to + " target='_blank'><i class='fa fa-linkedin-square'></i></a>" : "" )
			$( '.social li.linkedin' ).css('display', to ? 'inline-block' : 'none' );
		} );
	} );
	
	// Update the Google Plus link in real time...
	wp.customize( 'ledda_options[gplus_url]', function( value ) {
		value.bind( function( to ) {
			$( '.social li.google-plus' ).html( to ? "<a href=" + to + " target='_blank'><i class='fa fa-google-plus-square'></i></a>" : "" )
			$( '.social li.google-plus' ).css('display', to ? 'inline-block' : 'none' );
		} );
	} );
	
	// Update the Instagram link in real time...
	wp.customize( 'ledda_options[instagram_url]', function( value ) {
		value.bind( function( to ) {
			$( '.social li.instagram' ).html( to ? "<a href=" + to + " target='_blank'><i class='fa fa-instagram'></i></a>" : "" )
			$( '.social li.instagram' ).css('display', to ? 'inline-block' : 'none' );
		} );
	} );
	
	// Update the Instagram link in real time...
	wp.customize( 'ledda_options[youtube_url]', function( value ) {
		value.bind( function( to ) {
			$( '.social li.youtube' ).html( to ? "<a href=" + to + " target='_blank'><i class='fa fa-youtube-square'></i></a>" : "" )
			$( '.social li.youtube' ).css('display', to ? 'inline-block' : 'none' );
		} );
	} );
	
	// Update the Instagram link in real time...
	wp.customize( 'ledda_options[copyright_text]', function( value ) {
		value.bind( function( to ) {
			$( 'footer .footer-copyright' ).html( to );
		} );
	} );
	
} )( jQuery );