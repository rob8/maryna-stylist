<?php get_header(); ?>

	<header>
		
		<div class="container">
			
			<div class="row">
				
				<div class="col-xs-12">
					
					<div class="inner">
						
						<div class="inner-content">
					
							<hgroup>
								<h1>News &amp; Updates</h1>
								<?php
									
									$title = get_the_archive_title();
		 
								    if ( (!empty($title)) && ($title != 'Archives') ) {
								        echo '<h2>'.$title.'</h2>';
								    } elseif (is_search()) {
									   echo '<h2>Search: '; the_search_query().'</h2>';
								    } else { 
									    
									}
									
								?>
							</hgroup>
							
						</div> <!-- inner-content -->
					
					</div> <!-- inner -->
					
				</div> <!-- col-xs-12 -->
				
			</div> <!-- row -->
			
		</div> <!-- container -->
		
	</header>
	
	<div class="page-content">
		
		<div class="container">
			
			<div class="row">
				
				<main class="col-xs-12 col-sm-8 col-md-9">
					
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					
						<article>
							
							<div class="inner">
								
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								<div class="entry-meta"><span class="date"><?php the_time('l, F j, Y'); ?></span></div>
								
								<div class="entry-content">
									
									<?php the_excerpt(); ?>
									
								</div> <!-- entry-content -->
								
								<a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More...</a>
								
							</div> <!-- inner -->
							
						</article>
					
					<?php endwhile; else: ?>
					<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
					<?php endif; ?>
					
				</main>
				
				<div class="col-xs-12 col-sm-4 col-md-3">
					
					<?php get_sidebar(); ?>
					
				</div>
				
			</div> <!-- row -->
			
		</div> <!-- container -->
		
	</div>
		
<?php get_footer(); ?>