<!DOCTYPE html>
<html lang="en">
	
	
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		
		<?php wp_head(); //Loads head items generated by Wordpress ?>
		
		<script src="https://use.typekit.net/rkn0rqo.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
		
		<meta name="theme-color" content="#EE4877">
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	
	
	<body <?php body_class( $class ); ?>> 
		<div class="container">
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 single-work-logo">
					<a href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo('template_directory') ?>/images/logo-single-work.png" alt="logo-single-work" width="1078" height="180" class="img-responsive"></a>
				</div>
			</div>
			
			<div class="row">
				
				<div class="col-xs-10 col-xs-offset-1 col-md-12 col-md-offset-0">
					
					<div class="row">
				
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					
					<div class="col-xs-12 col-sm-6 col-md-3 work-item hvr-float-shadow">
				
						<div class="inner">
							
							<a class="inner-content" href="<?php the_permalink(); ?>">
								
								<?php 
									if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
										$params = array( 'width' => 550, 'height' => 550 );
										echo "<img src='" . bfi_thumb( get_the_post_thumbnail_url($post->ID, full), $params ) . "' class='img-responsive' />";
									} 
								?>
																	
							</a> <!-- .inner-content -->
							
						</div> <!-- .inner -->
						
					</div> <!-- .work-item -->
					
				<?php endwhile; else: ?>
				<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
				<?php endif; ?>
				
				</div></div>
				
			</div> <!-- row -->
			
		</div> <!-- container -->
		

		
		<?php wp_footer(); ?>
		
	</body>
	
</html>