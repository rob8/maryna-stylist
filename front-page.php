<?php get_header(); ?>

	<?php GLOBAL $ledda_options; ?>

	<header>
		<div class="shade">
			<div class="container">
				
				<div class="row">
					
					<div class="col-xs-11 col-md-6 col-lg-6">
						
						<h1>Providing <strong>hair &amp; makeup</strong> services specifically tailored to <strong>you</strong>. Every day should be a <strong>great hair day</strong>!</h1>
						
					</div>
					
				</div>
				
			</div>
		</div>
		
	</header>
	
	<div id="home-intro">
		
		<div class="container-fluid hidden-xs hidden-sm tools-image">
				
			<div class="col-xs-12 col-sm-5 col-sm-offset-7 tools-image-spot">
				
				&nbsp;
				
			</div>
			
		</div>
		
		<div class="container">
			
			<div class="row">
				
				<div class="col-xs-12 col-md-7">
					
					<h2>Elegant Style. <strong>Beautiful Execution.</strong></h2>
					
					<div class="intro-copy">
					
						<p>Looking your absolute best should be a daily experience. As a hair stylist, my mission is not only to find your perfect look, but also to empower you with the ability to pull it off every morning after your appointment. Because a hair cut is much more than just having your hair trimmed. It’s getting to know you, what you want, and what you need to maintain that perfect look every day.</p>
					
					</div>
					
					<a class="btn btn-primary btn-lg hvr-grow" href="#home-contact">Get In Touch</a>
					
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<div id="home-services">
		
		<div class="container">
			
			<div class="row">
				
				<div class="col-xs-12">
					
					<h2>My Services</h2>
					
				</div>
				
			</div>
			
			<div class="row services">
				
				<?php 
					$args = array( 'posts_per_page' => 999, 'post_type' => 'services', 'orderby' => 'menu_order' );
					
					$myservices = get_posts( $args );
					
					foreach ( $myservices as $post ) : setup_postdata( $post ); ?>
					
						<?php 
							$icon = get_post_meta($post->ID, 'maryna_services_image', true);
							$shortdesc = get_post_meta($post->ID, 'maryna_services_shortdesc', true);
						?>
					
						<div class="col-md-4 service">
					
							<div class="inner">
						
								<div class="inner-content">
									
									<div class="inner-content-top" data-mh="services-group">
									
										<?php echo ( !empty($icon) ? '<img src="'.$icon.'" height="70" alt="'.get_the_title($post->ID).'" />' : '' ); ?>
										
										<h3><?php the_title(); ?></h3>
										
										<p class="desc"><?php echo $shortdesc; ?></p>
									
									</div>
									
									<!-- <a class="btn btn-sm btn-primary hvr-grow" type="button" data-toggle="modal" data-target="#serviceModal<?php echo $post->ID; ?>">Learn More</a> -->
									
								</div> <!-- .inner-content -->
						
							</div> <!-- .inner -->
					
						</div> <!-- service -->
						
						<!-- Modal -->
						<div class="modal fade" id="serviceModal<?php echo $post->ID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&nbsp;</span></button>
										<h4 class="modal-title" id="myModalLabel"><?php the_title(); ?></h4>
									</div>
									<?php 
										if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
											$params = array( 'width' => 800, 'height' => 300 );
											echo "<img src='" . bfi_thumb( get_the_post_thumbnail_url($post->ID, full), $params ) . "' class='img-responsive' />";
										} 
									?>
									<div class="modal-body">
										<?php the_content(); ?>
									</div>
								</div>
							</div>
						</div>
					
					<?php endforeach; 
					wp_reset_postdata();
				?>
				
			</div> <!-- services -->
			
		</div>
		
	</div>
	
	<div id="home-work">
		
		<div class="container">
			
			<div class="row">
				
				<div class="col-xs-12 col-md-4">
					
					<h2>Recent <strong>Work</strong></h2>
					
				</div>
				
				<div class="col-xs-12 col-md-8 text-right instructions">
					
					<p class=""><i class="fa fa-arrows" aria-hidden="true"></i> Drag from left to right to see more photos. Select a photo to zoom in!</p>
					
				</div>
				
			</div> <!-- row -->
			
			<div class="row work-items">
				
				<div class="col-xs-12">
					
					<div class="owl-carousel">
				
				
				<?php 
					$args = array( 'posts_per_page' => 999, 'post_type' => 'work', 'orderby' => 'menu_order', 'order' => 'ASC' );
					
					$myservices = get_posts( $args );
					
					foreach ( $myservices as $post ) : setup_postdata( $post ); ?>
					
						<div class="work-item">
					
							<div class="inner">
								
								<a class="inner-content" data-toggle="modal" data-target="#workModal<?php echo $post->ID; ?>">
									
									<?php 
										if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
											$params = array( 'width' => 550, 'height' => 550 );
											echo "<img src='" . bfi_thumb( get_the_post_thumbnail_url($post->ID, full), $params ) . "' class='img-responsive' />";
										} 
									?>
																		
								</a> <!-- .inner-content -->
								
							</div> <!-- .inner -->
							
						</div> <!-- .work-item -->
						
					
					<?php endforeach; 
					wp_reset_postdata();
				?>
				
					</div>
				</div>
				
				
				
				<!-- 
					<div class="col-xs-12 col-sm-6 col-md-3 work-item hvr-float-shadow work-item-archive">
						
						<div class="inner">
							
							<a href="<?php echo get_post_type_archive_link( 'work' ); ?>" class="inner-content" data-mh="my-group">
								
								<div class="link-content">
									<i class="fa fa-scissors" aria-hidden="true"></i>
									<h3><strong>See More </strong>Of My Work</h3>
								</div>
								
							</a> 
							
						</div>
						
					</div> 
				-->
			
				
			</div> <!-- work-items -->
			
		</div> <!-- container -->
		
	</div> <!-- #home-services -->
	
	
	
	<?php 
		$args = array( 'posts_per_page' => 999, 'post_type' => 'work', 'orderby' => 'menu_order', 'order' => 'ASC' );
		
		$myservices = get_posts( $args );
		
		foreach ( $myservices as $post ) : setup_postdata( $post ); ?>
		
		<!-- Modal -->
			<?php $quote = get_post_meta($post->ID, 'maryna_work_quote', true); ?>
			<div class="modal fade" id="workModal<?php echo $post->ID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&nbsp;</span></button>
							<h4 class="modal-title" id="myModalLabel">Recent Work</h4>
						</div>
						<?php 
							if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
								$params = array( 'width' => 800, 'height' => 800 );
								echo "<img src='" . bfi_thumb( get_the_post_thumbnail_url($post->ID, full), $params ) . "' class='img-responsive' />";
							} 
						?>
						<!-- <div class="modal-body">
							<?php echo ( !empty($quote) ? '<blockquote><p>'.$quote.'</p></blockquote>' : '' ); ?>
							<a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More</a>
						</div> -->
					</div>
				</div>
			</div>
		
		<?php endforeach; 
		wp_reset_postdata();
	?>

<?php get_footer(); ?>